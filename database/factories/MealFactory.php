<?php

use Faker\Generator as Faker;

$factory->define(App\Meal::class, function (Faker $faker) {
    return [

        'name' => $faker->text(20),
        'date' => $faker->unique()->dateTimeBetween('next Monday', '+7 days')
    ];
});
