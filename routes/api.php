<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('meal', 'MealController@index'); //index
Route::get('meal/{id}', 'MealController@show');//show
Route::post('meal', 'MealController@store');//create
Route::put('meal', 'MealController@store');//update
Route::delete('meal/{id}', 'MealController@destroy');//delete
