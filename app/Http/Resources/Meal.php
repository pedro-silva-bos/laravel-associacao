<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Meal extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);

        return [
          'id' => $this->id,
          'name' =>  $this->name,
          'date' => $this->date
        ];
    }
    public function with($request){
      return[
        'version'=> '1.0.0',
        'author'=>url('www.google.com')
      ];
    }
}
