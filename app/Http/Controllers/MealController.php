<?php

namespace App\Http\Controllers;

use App\Http\Resources\Meal as MealResource;
use Illuminate\Http\Request;
use App\Meal;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
class MealController extends Controller
{
    //  public function __construct()
    // {
    //     $this->middleware('auth:admin', ['except'=>['index','show']]);
    // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //order by ascending date
        $meals = Meal::orderBy('date','desc')->paginate(7);
        return MealResource::collection($meals);
        //return view('meals.indexMeal')->with('meals', $meals);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $meal = $request->isMethod('put') ? Meal::findOrFail
      ($request->id) : new Meal;
      $meal->name = $request->input('name');
      $meal->date = $request->input('date');

      if($meal->save()){
        return new MealResource($meal);
      }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function addToCart($id)
    {

        return Redirect::to('meal/create');
    }
    public function show($id)
    {
        //get meal
        $meal = Meal::findOrFail($id);
        return new MealResource($meal);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $meals = Meal::find($id);
       return view('meals.editMeal')->with('meals',$meals);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'name'       => 'required',
            'date'      => 'required|date',

        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('meal/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $meal = Meal::find($id);
            $meal->name       = Input::get('name');
            $meal->date      = Input::get('date');

            $meal->save();

            // redirect
            Session::flash('message', 'Successfully created nerd!');
            return Redirect::to('meal/create');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         // delete
        $meal = Meal::findOrFail($id);
        if($meal->delete()){
          return new MealResource($meal);
        }


    }
}
