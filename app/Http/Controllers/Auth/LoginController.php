<?php

namespace App\Http\Controllers\Auth;
use File;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use App\User;
use Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    /**
    * Redirect the user to the Facebook authentication page.
    *
    * @return \Illuminate\Http\Response
    */
    public function redirectToFacebookProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from Facebook.
     *
     * @return void
     */
     public function handleProviderFacebookCallback()
    {
        $user = Socialite::driver('facebook')->user(); // Fetch authenticated user
        //this part takes care of the facebook avatar
        $fileContents = file_get_contents($user->getAvatar());
        File::put(public_path() . '/uploads/profile/' . $user->getId() . ".jpg", $fileContents);
        $picture = url('uploads/profile/' . $user->getId() . ".jpg");
        
        $user = User::updateOrCreate(
        [
            'email' => $user->email
        ],
        [
            'token' => $user->token,
            'name'  =>  $user->name,
            'avatar' => $picture
        ]
    );

    Auth::login($user, true);
    return redirect('/home'); // Redirect to a secure page
    }
}
