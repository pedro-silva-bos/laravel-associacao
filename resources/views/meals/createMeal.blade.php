@extends('layouts.app')
@section('content')
<div style="float: left;">
<h1>Create Meal</h1>
 <div class="form-group" >
       <form action="{{route('meal.store')}}" method="POST" id="createMealID">
       	@csrf
       	<div class="form-group">
       		<label>Data</label>
       		<input type="date" name="date">
       	</div>
   		<div class="form-group">
   			<label>Nome</label>
    		<textarea rows="4" cols="50" name="name" >
    		</textarea>
		</div>
  		<input type="submit" value="Submit">
       </form>
    	
   
    </div>
</div>
    <div style="float: right;padding-right: 400px">
    <table> @foreach($meals as $key => $value)
     
        <tr>
            <td>{{ $value->date }}</td>
            <td>{{ $value->name }}</td>



            <!-- we will also add show, edit, and delete buttons -->
            <td>

             {{ Form::open(array('route' => array('meal.destroy', $value->id), 'method' => 'delete')) }}
				    <button class="btn btn-small btn-danger" type="submit">Delete Meal</button>
				    

                <!-- edit this nerd (uses the edit method found at GET /meal/{id}/edit -->
                <a class="btn btn-small btn-info" href="{{ URL::to('meal/' . $value->id . '/edit') }}">Edit Meal</a>
				{{ Form::close() }}
                <!-- show the nerd (uses the show method found at GET /meal/{id} -->
                

            </td>
        </tr></div>
    @endforeach
</table>
@endsection