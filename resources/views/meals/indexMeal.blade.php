@extends('layouts.app')
@section('content')
<h1>Meal index</h1>
   <table> @foreach($meals as $key => $value)

        <tr>
            <td>{{ $value->id }}</td>
            <td>{{ $value->name }}</td>
<div id="app">
  <meals></meals>
</div>
            <!-- we will also add show, edit, and delete buttons -->
<td>
  <!-- show the meals (uses the show method found at GET /meal/{id} -->
  <a class="btn btn-small btn-success" href="{{route('meal.show' , $value->id)}}" >Show this Neddrd</a>
</td>
</tr>
    @endforeach
</table>
<script src="{{asset:js/app.js}}"> </script>
@endsection
